### What is this repository for? ###

This repository houses the jar files required to integrate CAS with your JIRA 7+ instance. Please click "Downloads" on the left hand column to download the JAR files.

You place these JAR files into the JIRA INSTALL DIRECTORY/atlassian-jira/WEB-INF/lib 

Additional questions regarding your CAS integration can be directed to info@ascendintegrated.com.

This code was compiled and built into a JAR file for anyone using CAS to authenticate with JIRA 7. 

For a full list of instructions on installing / configuring CAS with JIRA 7, go here: http://www.ascendintegrated.com/integrating-jira-sso-using-cas/

Thank you for downloading!